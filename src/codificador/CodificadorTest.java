package codificador;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;


@RunWith(Parameterized.class)
public class CodificadorTest {
	
	private String palabra;
	private String contrasenya;
	private String resul;
	
	private static final char[][] lletres = {{'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','Z'},
								{'B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','A'},
								{'C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','B'},
								{'D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','C'},
								{'E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','D'},
								{'F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','E'},
								{'G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','F'},
								{'H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','G'},
								{'I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','H'},
								{'J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','I'},
								{'K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','J'},
								{'L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','K'},
								{'M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','L'},
								{'N','O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','M'},
								{'O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','N'},
								{'P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','O'},
								{'Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','P'},
								{'R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','Q'},
								{'S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','R'},
								{'T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','S'},
								{'U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','T'},
								{'V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','U'},
								{'W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','V'},
								{'X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','W'},
								{'Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','X'},
								{'Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Y'}};
	
	
	public CodificadorTest(String palabra, String contrasenya, String resul) {
	
		this.palabra = palabra;
		this.contrasenya = contrasenya;
		this.resul = resul;
	}
	
	
	@Parameters
	public static Collection<Object[]> numeros() 
	{
		return Arrays.asList(new Object[][] 
		{
			{"HOLA", "KEY", "RSJK"},
			{"¿Hola que tal estas?", "ola", "¿SOZL EFE EAZ EGEAG?"},
			{"Harry Potter se ha quedado huerfano y vive en casa de sus abominables tios y del insoportable primo Dudley.", "Harry", "OAIIW PFKRLR JC HR OBEURBV YLCYFREM Y MGCE VL CRJY DV QBS RZVMZEYILVJ AIFJ F UVJ IEJMWOIKYILV NYIDF KUUCCF."},
			{"Lorem ipsum dolor sit amet.", "LoremIpsum", "WCIIY XHMGX USXWG MUE RQQB."},
			{"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ultricies.", "buenas", "MIVRM JJWHM EIPBR TCX AEFN, CGOMIPTWUOV AVJJMFCAOA RLAU. SUKDY HLLSCGVEK."}
		});		
	}
	
	@Test
	public void testTraductor() 
	{
		String res = Codificador.traductor(palabra.toUpperCase(), contrasenya.toUpperCase(), lletres);
		assertEquals(resul, res);
	}

}
