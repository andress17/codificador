package codificador;

import java.util.Scanner;

/**
 * <h2>Codificador</h2>
 * 
 * Utilitzant la codificació de Vigenère demanaràs una cadena de text de qualsevol longitud 
 * i sense diferenciar majúscules i minúscules, una contrasenya que s’utilitzarà per codificar 
 * la cadena de text inicial tampoc diferenciarà entre majúscules i minúscules. La contrasenya 
 * serà una cadena de text sense espais ni caràcters especials. 
 * El programa finalitzarà quan aquesta cadena sigui un 0. 
 * @author Jonathan Raya Rios
 * @since 10-03-19
 */


public class Codificador {
	public static void main(String[] args) 
	{
		String missatge = null;
		String contrasenya = null;
		Scanner reader = new Scanner(System.in);
		String res;
		char[][] lletres = new char[26][26]; 
		
		taulaLletres(lletres);
		do {
			missatge = reader.nextLine();
			missatge = missatge.toUpperCase();
			if (!missatge.equals("0"))
			{
				contrasenya = reader.nextLine();
				contrasenya = contrasenya.toUpperCase();
				res = traductor(missatge, contrasenya, lletres);
				System.out.println(res);
			}
		} while (!missatge.equals("0"));
		reader.close();
	}
	
	/**
	 * Metode que s'encarrega de generar la taula de lletres.
	 * @param lletres Matriu amb totes les lletrees del abecedari angles saltant-se una posicio.
	 */
	
	public static void taulaLletres(char[][] lletres) 
	{
		char letra;
		char aux = 'A'; 
		int i, j;
		for (i = 0; i < 26; i++)
		{
			letra = aux;
			for (j = 0; j < 26; j++)
			{
				lletres[i][j] = letra;
				letra++;
				if (letra > 'Z')
				{
					letra = 'A';
				}
			}
			aux++;
		}
	}
	
	/**
	 * Metode que s'encarrega d'encriptar el missatge utilitzant una matriu de lletres.
	 * @param missatge Missatge introduit per l'usuari.
	 * @param contrasenya Cadena de text introduida per l'usuari, s'utilitza per xifrar el missatge.
	 * @param lletres Matriu amb totes les lletrees del abecedari angles saltant-se una posicio.
	 * @return Retorna la paraula encriptada.
	 */
	
	public static String traductor(String missatge, String contrasenya, char[][] lletres) 
	{		
		int i = 0;
		int j = 0;
		StringBuilder resultat = new StringBuilder();
				
		while (resultat.length() < missatge.length())
		{
			if (missatge.charAt(j) < 'A' || missatge.charAt(j) > 'Z')
			{
				resultat.append(missatge.charAt(j));
			}
			else
			{
				resultat.append(lletres[contrasenya.charAt(i) - 'A'][missatge.charAt(j) - 'A']);
				i++;
			}		
			j++;
			if (i >= contrasenya.length())
			{
				i = 0;
			}
		}
		return resultat.toString();
	}
}
